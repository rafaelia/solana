// First lesson
// import { Keypair } from '@solana/web3.js';
// import { getKeypairFromEnvironment } from '@solana-developers/helpers';

// const keypair = Keypair.generate();
// console.log(`The public key is: `, keypair.publicKey.toBase58());
// console.log(`The secret key is: `, keypair.secretKey);
// console.log(`✅ Finished!`);

// Second lesson
// import 'dotenv/config';
// import { getKeypairFromEnvironment } from '@solana-developers/helpers';

// const keypair = getKeypairFromEnvironment('SECRET_KEY');

// console.log(
//   `✅ Finished! We've loaded our secret key securely, using an env file!`
// );

//Third lession:get balance of solana wallet
// import { Connection, PublicKey, clusterApiUrl } from '@solana/web3.js';

// const connection = new Connection(clusterApiUrl('devnet'));
// const address = new PublicKey('54gSi4NxvChwhAJ1H5v5HBTHxeGr2J7z6wbictbutn5c');
// const balance = await connection.getBalance(address);

// console.log(`The balance of the account at ${address} is ${balance} lamports`);
// console.log(`✅ Finished!`);

//-------------------->4
import {
  Connection,
  Transaction,
  SystemProgram,
  sendAndConfirmTransaction,
  PublicKey,
} from '@solana/web3.js';
import 'dotenv/config';
import { getKeypairFromEnvironment } from '@solana-developers/helpers';

const suppliedPubKey = process.argv[2] || null;

if (!suppliedPubKey) {
  console.log('Please provide key');
  process.exit(1);
}
const senderkeypair = getKeypairFromEnvironment('SECRET_KEY');
if (!senderkeypair) {
  console.log('Please set your SECRET_KEY in the environment');
  process.exit(1);
}

console.log(`suppliedPubkey: ${suppliedPubKey}`);
console.log(`sender's public key ${senderkeypair.publicKey}`);

const toPubKey = new PublicKey(suppliedPubKey);
const connect = new Connection('https://api.devnet.solana.com', 'confirmed');

console.log(
  `✅ Loaded our own keypair, the destination public key, and connected to Solana`
);

const transaction = new Transaction();

const LAMPORTS_TO_SEND = 5000000;

const sendSolInstruction = SystemProgram.transfer({
  fromPubkey: senderkeypair.publicKey,
  toPubkey: toPubKey,
  lamports: LAMPORTS_TO_SEND,
});

transaction.add(sendSolInstruction);

try {
  const signature = await sendAndConfirmTransaction(connect, transaction, [
    senderkeypair,
  ]);

  console.log(
    `💸 Finished! Sent ${LAMPORTS_TO_SEND} lamports to the address ${toPubKey}. `
  );
  console.log(`Transaction signature is ${signature}!`);
} catch (error) {
  console.error(
    'Error sending transaction--------------------------------?:',
    error
  );
}

